// SPDX-License-Identifier: BSD-3-Clause

/**
 * © 2020 Nicolas Guichard <nicolas.guichard@grenoble-inp.org>
 *
 * Adapted from the work of Oswald Buddenhagen <oswald.buddenhagen@gmx.de>
 * © 2002 and under the same licence:
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
**/
use std::env;
use std::os::unix::process::CommandExt;
use std::path::{Path, PathBuf};
use std::process::Command;

use is_executable::IsExecutable;

use log::debug;
use simplelog::{Config, LevelFilter, SimpleLogger, TermLogger, TerminalMode};

fn find_original(path: &str, program: &str, my_path: &Path) -> String {
    let original = env::split_paths(path)
        .map(|directory| directory.join(program))
        .filter(|file| file.exists())
        .filter(|file| &file.canonicalize().unwrap() != my_path)
        .find(|file| file.is_executable())
        .unwrap_or_else(|| panic!("Can't find {:?} in $PATH.", program))
        .to_str()
        .unwrap()
        .to_string();

    debug!("Found the original {:?} in {:?}", program, original);

    original
}

fn get_env(name: &str) -> String {
    env::var(name).unwrap_or(String::new())
}

fn parse_leading_int(string: &str) -> usize {
    string
        .split(|c: char| !c.is_numeric())
        .next()
        .unwrap()
        .parse()
        .unwrap()
}

fn get_job_count(flags: &str) -> usize {
    if flags.contains("dist") {
        let dist_flags = get_env("DISTCC_HOSTS");
        dist_flags.split_whitespace().count() + 1
    } else {
        match flags.find("ice=") {
            Some(pos) => parse_leading_int(&flags[pos + 4..]),
            None => match flags.find("jobs=") {
                Some(pos) => parse_leading_int(&flags[pos + 5..]),
                None => 1,
            },
        }
    }
}

fn find_ccache_dir(ccache_config: &str) -> Option<PathBuf> {
    let current_dir = env::current_dir().expect("No working directory");

    let mut options = ccache_config.split(':');

    options.next(); // Skip the ccache string

    let from_flags = options.find_map(|option| match option.find('=') {
        None => None,
        Some(equal) => {
            let when_dir = &option[..equal];
            let then_dir = &option[equal + 1..];

            if current_dir.starts_with(&when_dir) {
                let ccache_location = Path::new(then_dir);
                return Some(ccache_location.join(".ccache"));
            } else {
                None
            }
        }
    });

    match from_flags {
        Some(location) => Some(location),
        None => current_dir.ancestors().find_map(|ancestor| {
            let potential = ancestor.join(".ccache");
            if potential.is_dir() {
                Some(potential)
            } else {
                None
            }
        }),
    }
}

fn main() {
    let flags = get_env("WRAPCCFLAGS");

    if flags.contains("debug") {
        if let Err(_) = TermLogger::init(LevelFilter::Debug, Config::default(), TerminalMode::Mixed)
        {
            SimpleLogger::init(LevelFilter::Debug, Config::default())
                .expect("No logger should be already set")
        }
    }

    let path = env::var("PATH").expect("$PATH is not set!");

    let my_exe = env::current_exe().unwrap().canonicalize().unwrap();

    let mut my_args = env::args();
    let my_full_name = my_args.next().unwrap();
    let my_name = my_full_name.split('/').last().unwrap().to_string();

    debug!(
        "I was called as {:?}, but in reality I am {:?}",
        my_name, my_exe
    );
    debug!("My options are {:?}", flags);

    let mut args = Vec::new();

    let program = if my_name == "make" || my_name == "gmake" {
        let job_count = get_job_count(&flags);
        debug!("{} workers are required", job_count);

        if job_count > 1 {
            args.push(format!("-j{}", job_count));
        }

        if let Some(pos) = flags.find("cache") {
            let ccache_dir = find_ccache_dir(flags[pos..].split(',').next().unwrap());

            match ccache_dir {
                Some(dir) => {
                    debug!("Set CCACHE_DIR to {:?}", dir);
                    env::set_var("CCACHE_DIR", dir);
                }
                None => debug!("Did not find what to set CCACHE_DIR to, sorry"),
            }
        }

        find_original(&path, &my_name, &my_exe)
    } else {
        let name = match (my_name.contains('-'), flags.find("append=")) {
            (false, Some(p)) => {
                let suffix = flags[p + 7..].split(',').next().unwrap();
                let suffixed = format!("{}-{}", my_name, suffix);
                debug!(
                    "I was asked to add the suffix {:?}, so my name becomes {:?}",
                    suffix, suffixed
                );
                suffixed
            }
            _ => String::from(my_name),
        };

        if flags.contains("cache") {
            let ccache_prefix = if flags.contains("dist") {
                debug!("This is a ccache build assisted by distcc");
                Some("distcc")
            } else if flags.contains("icecc") {
                debug!("This is a ccache build assisted by icecc");
                Some("icecc")
            } else {
                debug!("This is a pure ccache build");
                None
            };

            if let Some(ccache_prefix) = ccache_prefix {
                let ccache_prefix = find_original(&path, ccache_prefix, &my_exe);

                debug!("Setting CCACHE_PREFIX to {:?}", ccache_prefix);

                env::set_var("CCACHE_PREFIX", ccache_prefix);
            }

            let fd = unsafe { libc::dup(2) };
            debug!("Setting UNCACHED_ERR_FD to {:?}", fd);
            env::set_var("UNCACHED_ERR_FD", fd.to_string());

            args.push(find_original(&path, &name, &my_exe));
            String::from("ccache")
        } else {
            let real_program = if flags.contains("dist") {
                Some("distcc")
            } else if flags.contains("ice") {
                Some("icecc")
            } else {
                None
            };

            match real_program {
                Some(program) => {
                    args.push(find_original(&path, &name, &my_exe));
                    String::from(program)
                }
                None => find_original(&path, &name, &my_exe),
            }
        }
    };

    args.extend(&mut my_args);

    debug!(
        "I'm going to execute {:?} with arguments {:?}",
        program, args
    );

    let error = Command::new(program).args(args).exec();

    panic!("{}", error);
}
